package com.humanbooster.security.project.demo.services;

import com.humanbooster.security.project.demo.models.Role;
import com.humanbooster.security.project.demo.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public void saveRole(Role role){
        this.roleRepository.save(role);
    }

    public Role getRoleByNom(String roleName){
        return this.roleRepository.findByRoleName(roleName);
    }
}
