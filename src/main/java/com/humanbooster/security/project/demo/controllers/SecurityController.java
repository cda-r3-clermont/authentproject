package com.humanbooster.security.project.demo.controllers;

import com.humanbooster.security.project.demo.models.User;
import com.humanbooster.security.project.demo.services.UserService;
import com.humanbooster.security.project.demo.validationgroups.RegisterGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping()
public class SecurityController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    ModelAndView login(){
        // Si tu es connecté tu rediriges !
        ModelAndView mv = new ModelAndView("security/login.html");
        return mv;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    ModelAndView logout(){
        ModelAndView mv = new ModelAndView("security/logout");
        return mv;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    ModelAndView register(){
        User user = new User();
        ModelAndView mv = new ModelAndView("security/register");
        mv.addObject("user", user);

        return mv;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    String register(@Validated(RegisterGroup.class) User user,
                    BindingResult bindingResult, Model model){

        User userExist = userService.findByUsername(user.getUsername());

        if(bindingResult.hasErrors() || userExist != null){
            if(userExist != null){
                model.addAttribute("userExist", true);
            }

            return "security/register";
        } else {
            userService.save(user);

            return "redirect:/login";
        }
    }
}
