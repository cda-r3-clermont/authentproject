package com.humanbooster.security.project.demo.services;

import com.humanbooster.security.project.demo.constraint.FieldMatch;
import com.humanbooster.security.project.demo.models.Role;
import com.humanbooster.security.project.demo.models.User;
import com.humanbooster.security.project.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpringAuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username);

        if(user == null){
            throw new UsernameNotFoundException("Utilisateur ou mot de passe incorrecte !");
        }

        List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();

        for (Role role : user.getRoles()){
            GrantedAuthority ga = new SimpleGrantedAuthority(role.getRoleName());
            roles.add(ga);
        }



        return  new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), roles);
    }
}
