package com.humanbooster.security.project.demo;


import com.humanbooster.security.project.demo.models.Role;
import com.humanbooster.security.project.demo.models.User;
import com.humanbooster.security.project.demo.services.RoleService;
import com.humanbooster.security.project.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class DataCreatorApplication {



    public static void main(String[] args){
        SpringApplication.run(DataCreatorApplication.class);
    }

    @Bean
    public CommandLineRunner dataLoader(UserService userService, RoleService roleService){
        return args -> {
            Role roleAdmin = roleService.getRoleByNom("admin");
            Role roleUser =   roleService.getRoleByNom("user");

            if(roleAdmin == null){
                 roleAdmin = new Role("admin");
                 roleService.saveRole(roleAdmin);
            }

            if(roleUser == null){
                roleUser = new Role("user");
                roleService.saveRole(roleUser);
            }

            if(userService.findByUsername("user") == null){
                User user = new User();
                user.setUsername("user");
                user.setPassword("user");
                user.setFirstname("User");
                user.setLastname("User");

                user.addRole(roleUser);

                userService.save(user);
            }

            if(userService.findByUsername("admin") == null){
                User user = new User();
                user.setUsername("admin");
                user.setPassword("admin");
                user.setFirstname("Admin");
                user.setLastname("Admin");

                user.addRole(roleUser);
                user.addRole(roleAdmin);

                userService.save(user);
            }

        };
    }
}
