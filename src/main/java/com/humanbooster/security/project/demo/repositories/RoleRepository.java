package com.humanbooster.security.project.demo.repositories;

import com.humanbooster.security.project.demo.models.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByRoleName(String role);
}
